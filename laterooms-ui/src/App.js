import React, { Component } from "react";
import DataService from "./dataService";

import "bootstrap/dist/css/bootstrap.min.css";
import "font-awesome/css/font-awesome.min.css";

import "./App.css";

import HotelList from "./HotelList/hotellist.component";
import FilterPanel from "./FilterPanel/filterpanel.component";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [] };
  }

  componentWillMount() {
    var ds = new DataService();
    var data = ds.getData();

    this.setState({ data: data });

    console.log(this.state.data);
  }

  render() {
    return (
      <div className="App container">
        <div className="row">
          <div className="col">
            <FilterPanel
              onUpdate={this.onUpdate.bind(this)}
              hotels={this.state.data}
            />
          </div>
          <div className="col-12 col-sm-12 col-md-12 col-lg-8">
            <br />
            <HotelList hotels={this.state.data} />
          </div>
        </div>
      </div>
    );
  }

  onUpdate(data) {
    this.setState({ data: data });
  }
}

export default App;
