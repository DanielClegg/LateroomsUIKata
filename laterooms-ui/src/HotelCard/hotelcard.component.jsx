import React, { Component } from "react";

import Facilities from "../Facilities/facilities.component";
import Stars from "../Stars/stars.component.jsx";

export default class HotelCard extends Component {
  render() {
    return (
      <div className="card ">
        <div className="card-header">
          <h5>{this.props.hotel.Name}</h5>
        </div>
        <div className="card-body">
          <img
            alt={this.props.hotel.Name}
            src={"https://placeimg.com/140/140/arch?" + this.props.hotel.Name}
            className=" float-right"
          />
          <h5 className="card-title">
            Star rating {this.props.hotel.StarRating}
          </h5>
          <p className="card-text" />
          <Facilities facs={this.props.hotel.Facilities} />

          <a href="" className="btn btn-primary">
            {/*            <span>
              Book the <Stars stars={this.props.hotel.StarRating} />{" "}
              {this.props.hotel.Name}
            </span>*/}
          </a>
        </div>
      </div>
    );
  }
}
