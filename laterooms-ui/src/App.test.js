import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import DataService from "./dataService";

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
});

it("get data from service", () => {
    var dataService = new DataService();
    var data = dataService.getData();
    expect(data.length).toEqual(9);
});

it("sort data by star rating asc from service", () => {
    var dataService = new DataService();
    var sortedData = dataService.getData(DataService.HIGHEST);

    expect(sortedData[0].Name).toEqual("hotelone");
    expect(sortedData.length).toEqual(9);
});

it("sort data by star rating desc from service", () => {
    var dataService = new DataService();
    var sortedData = dataService.getData(DataService.LOWEST);

    expect(sortedData[0].Name).toEqual("hotel 8");
    expect(sortedData.length).toEqual(9);
});

it("filter data by carpark from service", () => {
    var dataService = new DataService();
    var sortedData = dataService.getDataFilter("car park");

    expect(sortedData[0].Name).toEqual("hotelone");
    expect(sortedData.length).toEqual(6);
});

it("filter data by gym from service", () => {
    var dataService = new DataService();
    var sortedData = dataService.getDataFilter("gym");

    expect(sortedData[0].Name).toEqual("hoteltwo");
    expect(sortedData.length).toEqual(3);
});

it("filter data by pool from service", () => {
    var dataService = new DataService();
    var sortedData = dataService.getDataFilter("pool");

    expect(sortedData[0].Name).toEqual("hotelone");
    expect(sortedData.length).toEqual(3);
});
