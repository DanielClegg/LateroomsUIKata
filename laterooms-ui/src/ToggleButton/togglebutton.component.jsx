import React, { Component } from "react";

import "./togglebutton.component.css";

export default class ToggleButton extends Component {
	constructor(props) {
		super(props);

		this.state = {
			clicked: false
		};
	}

	togglebtn() {
		this.setState({ clicked: !this.state.clicked });
		this.props.onClick(this.props.text);
	}

	render() {
		var className =
			this.state.clicked === true
				? "btn btn-warning btn-space"
				: "btn btn-outline-warning btn-space";

		return (
			<button onClick={this.togglebtn.bind(this)} className={className}>
				{this.props.text}
			</button>
		);
	}
}
