import React, { Component } from "react";

import HotelCard from "../HotelCard/hotelcard.component";

export default class HotelList extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    var list = this.props.hotels.map((hotel, i) => {
      return (
        <div className="col-12">
          <HotelCard key={i} hotel={hotel} />
          <br />
        </div>
      );
    });
    return list;
  }
}
