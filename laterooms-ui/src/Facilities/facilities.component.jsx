import React, { Component } from "react";

export default class Facilities extends Component {
  render() {
    var facs = this.props.facs.map((fac, i) => {
      var carp = (
        <li key={i}>
          <span className="badge badge-warning">{fac}</span>{" "}
        </li>
      );

      return carp;
    });

    return (
      <div>
        {this.props.facs.length > 0 ? (
          <ul className="list-unstyled">{facs}</ul>
        ) : (
          <span>
            <span>No facilities available at this hotel</span>
            <br />
          </span>
        )}
      </div>
    );
  }
}
