import React, { Component } from "react";

export default class Stars extends Component {
	render() {
		var renderStars = [];
		for (var i = 0; i < this.props.stars; i++) {
			renderStars.push(<i className="fas fa-star" key={i} />);
		}

		return renderStars;
	}
}
