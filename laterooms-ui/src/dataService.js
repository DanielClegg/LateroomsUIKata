export default class DataService {
    constructor() {
        this.facilities = [
            {
                Name: "Gym"
            },
            {
                Name: "Car Park"
            },
            {
                Name: "Pool"
            }
        ];

        this.data = [
            {
                Name: "The Big Western",
                StarRating: 5,
                Facilities: ["car park", "pool"]
            },
            {
                Name: "Hotel Grovey",
                StarRating: 3,
                Facilities: ["car park", "gym"]
            },
            {
                Name: "The HollyBush",
                StarRating: 3,
                Facilities: []
            },
            {
                Name: "Hotel Four",
                StarRating: 5,
                Facilities: ["car park", "pool"]
            },
            {
                Name: "Posh Rooms",
                StarRating: 2,
                Facilities: ["car park", "gym"]
            },
            {
                Name: "The Flamingo",
                StarRating: 4,
                Facilities: []
            },
            {
                Name: "The Hiltron",
                StarRating: 2,
                Facilities: ["car park", "pool"]
            },
            {
                Name: "Bingo Rooms",
                StarRating: 1,
                Facilities: ["car park", "gym"]
            },
            {
                Name: "Alice Block",
                StarRating: 5,
                Facilities: []
            }
        ];
    }

    getFacs() {
        return this.facilities;
    }

    getDataFilter(filter) {
        var results = [];

        if (filter === "") return this.getData();

        results = this.data.filter(hotel => hotel.Facilities.includes(filter));

        if (results.length === 0) return [];
        return results;
    }

    getDataFilter2(filter) {
        console.log(filter);

        if (filter.length === 0) return this.getData();
        for (var i = 0; i < filter.length; i++) {
            filter[i] = filter[i].toLowerCase();
        }

        var results = [];

        for (var i = 0; i < this.data.length; i++) {
            for (var j = 0; j < filter.length; j++) {
                if (this.data[i].Facilities.includes(filter[j])) {
                    results.push(this.data[i]);
                    break;
                }
            }
        }

        if (results.length === 0) return [];
        return results;
    }

    getData(sort) {
        var results = [];

        switch (sort) {
            case undefined: {
                results = this.data;
                break;
            }
            case 1: {
                results = this.data.sort((x, y) => y.StarRating - x.StarRating);
                break;
            }
            case 2: {
                results = this.data.sort((x, y) => x.StarRating - y.StarRating);
                break;
            }
            default: {
                results = this.data;
                break;
            }
        }
        return results;
    }

    static HIGHEST = 1;
    static LOWEST = 2;
}
