import React, { Component } from "react";

import Title from "../Title/title.component";
import DataService from "../dataService";
import ToggleButton from "../ToggleButton/togglebutton.component";

export default class FilterPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      asc: "btn btn-secondary btn-block",
      desc: "btn btn-secondary btn-block",
      ds: new DataService()
    };

    this.filterList = [];
  }

  componentWillMount() {}

  onSort(sort) {
    var data = this.state.ds.getData(sort);

    this.props.onUpdate(data);

    if (sort === 1)
      this.setState({
        asc: "btn btn-primary btn-block",
        desc: "btn btn-secondary btn-block"
      });
    else
      this.setState({
        desc: "btn btn-primary btn-block",
        asc: "btn btn-secondary btn-block"
      });
  }

  onFilter(event) {
    var data = this.state.ds.getDataFilter(event.target.value);

    this.props.onUpdate(data);
  }

  facButtonClick(name) {
    var index = this.filterList.indexOf(name.toLowerCase());
    if (index === -1) {
      this.filterList.push(name);
    } else {
      this.filterList.splice(index, 1);
    }

    var data = this.state.ds.getDataFilter2(this.filterList);

    this.props.onUpdate(data);
  }

  render() {
    var facButtons = this.state.ds.getFacs().map((item, i) => {
      return (
        <ToggleButton
          key={i}
          text={item.Name}
          onClick={this.facButtonClick.bind(this)}
        />
      );
    });

    return (
      <div className="sticky">
        <Title text="Laterooms UI Kata" />
        <ul className="list-unstyled">
          <li>
            <button
              onClick={this.onSort.bind(this, 1)}
              className={this.state.asc}
            >
              Sort hotels by most stars
            </button>
          </li>
          <br />
          <li>
            <button
              onClick={this.onSort.bind(this, 2)}
              className={this.state.desc}
            >
              Sort hotels by the least stars
            </button>
          </li>
          <br />
          <li>
            <div className="input-group mb-3">
              <div className="input-group-prepend">
                <span className="input-group-text" id="basic-addon1">
                  <i className="fas fa-filter" />
                </span>
              </div>
              <input
                type="text"
                className="form-control"
                placeholder="Facilities filter"
                aria-label="Username"
                aria-describedby="basic-addon1"
                onChange={this.onFilter.bind(this)}
              />
            </div>
          </li>
        </ul>
        {facButtons}
      </div>
    );
  }
}
